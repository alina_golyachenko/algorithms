﻿using System;
using System.Text;

namespace Kontr2
{
    class Program
    {

        //количество вершин
        public static int count = 10;
        public static int[,] graph = new int[count, count];

        //выходной массив
        //в dist[i] будет кратчайшая дистанция от v1 до i
        public static int[] dist = new int[count];
        public static bool[] visited = new bool[count];

        //ищем элемент с минимальной дистанцией (его индекс)
        public static int MinDistance(int[] dist, bool[] visited)
        {
            int min = int.MaxValue;
            int minIndex = -1;

            for (int i = 0; i < count; i++)
            {
                if (visited[i] == false && dist[i] <= min)
                {
                    min = dist[i];
                    minIndex = i;
                }
            }
            return minIndex;
        }

        public static void PrintDistances(int src)
        {
            Console.Write($"\tПочатковий елемент: {src}\n\n\tВідстань від обраного елементу до усіх інших:\n\n");
            for (int i = 0; i < count; i++)
            {
                if (dist[i] != Int32.MaxValue)
                {
                    Console.Write($"\t{src} -> {i} : {dist[i]}\n");
                }
            }
        }

        public static void Dijkstra(int start, int end)
        {
            string path = "";
            //все дистанции у нас бесконечность,
            //а все вершины не были посещены
            for (int i = 0; i < count; i++)
            {
                dist[i] = Int32.MaxValue;
                visited[i] = false;
            }

            //дистанция начальной вершины
            //её расстояние до самой себя всегда == 0
            dist[start] = 0;

            // находим кратчайший путь для каждой вершины

            for (int i = 0; i < count - 1; i++)
            {
                //берём вершину с минимальной дистанцией (индекс)
                //на первой итерации current всегда равна start
                int current = MinDistance(dist, visited);

                //помечаем вершину как посещённую
                visited[current] = true;

                //обновляем дистанции до каждой соседней вершины
                for (int j = 0; j < count; j++)
                {
                    //мы можем обновить дистанцию только если:
                    //  - вершина не была посещена
                    //  - есть ребро от current до j
                    //  - вес ребра от start до j через current меньше,
                    //    чем текущее значение dist[j]

                    if (!visited[j] && graph[current, j] != 0 &&
                         dist[current] != int.MaxValue &&
                         dist[current] + graph[current, j] < dist[j])
                    {
                        dist[j] = dist[current] + graph[current, j];
                    }
                }
            }
            Console.WriteLine($"\n\t{path}");
        }


        public static string GetPath(int start, int end)
        {
            //массив вершин
            int[] vertices = new int[count];

            vertices[0] = end;
            //индекс предыдущей вершины
            int k = 1;
            int weight = dist[end];

            while (end != start)
            {
                for (int i = 0; i < count; i++)
                {
                    if (graph[i, end] != 0)
                    {
                        int temp = weight - graph[i, end]; // определяем вес пути из предыдущей вершины

                        //если вес совпал с рассчитанным, то с этой вершины был сделан переход
                        if (temp == dist[i])
                        {
                            weight = temp; // сохраняем новый вес
                            end = i;
                            //сохраняем предыдущую вершину и записываем ее в массив
                            vertices[k] = i;
                            k++;
                        }
                    }
                }
                    
            }

            string path = "";

            for (int i = k - 1; i >= 0; i--)
            {
                path += vertices[i].ToString() + " ";
            }

            return path;
        }

        static void Main(string[] args)
        {
            graph[0, 1] = 3;
            graph[0, 2] = 4;
            graph[0, 3] = 2;
            graph[1, 5] = 3;
            graph[2, 5] = 6;
            graph[3, 4] = 5;
            graph[3, 5] = 2;
            graph[4, 6] = 6;
            graph[4, 8] = 12;
            graph[5, 4] = 1;
            graph[5, 7] = 7;
            graph[6, 9] = 4;
            graph[7, 9] = 3;
            graph[8, 7] = 6;
            graph[8, 9] = 11;

            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            Console.WriteLine("\n\t1 - знайти найкоротший шлях від обраного елементу до всіх інших (відстань)");
            Console.Write("\n\t2 - знайти найкоротший шлях від одного елементу до іншого");
            Console.WriteLine("\n\n\tРахунок від 0!");
            Console.Write("\n\tОберіть дію: ");

            int menu = Int32.Parse(Console.ReadLine());

            switch (menu)
            {
                case 1:
                    {
                        Console.Write("\n\tЗнайти найкоротший шлях від елемента № ");
                        int start = Int32.Parse(Console.ReadLine());
                        Console.Write("\tдо всіх елементів:");
                        Dijkstra(start, count - 1);
                        PrintDistances(start);

                        break;
                    }
                case 2:
                    {
                        Console.Write("\n\tЗнайти найкоротший шлях від елемента: ");
                        int start = Int32.Parse(Console.ReadLine());
                        Console.Write("\tдо елемента: ");
                        int end = Int32.Parse(Console.ReadLine());
                        Dijkstra(start, end);
                        string path = GetPath(start, end);

                        Console.Write($"\tНайкоротший шлях від {start} до {end}: {path} \n\n");


                        break;
                    }
            }

        }



    }
}
