﻿#include <iostream>
#include <windows.h>
#include <time.h>
#include <string.h>

union SignedShort
{
    signed short a;
    struct
    {
        struct
        {
            unsigned char b0 : 1;
            unsigned char b1 : 1;
            unsigned char b2 : 1;
            unsigned char b3 : 1;
            unsigned char b4 : 1;
            unsigned char b5 : 1;
            unsigned char b6 : 1;
            unsigned char b7 : 1;
                     
        } byte0Var;  
        struct       
        {            
            unsigned char b8 : 1;
            unsigned char b9 : 1;
            unsigned char b10 : 1;
            unsigned char b11 : 1;
            unsigned char b12 : 1;
            unsigned char b13 : 1;
            unsigned char b14 : 1;
            unsigned char b15 : 1;

        } byte1Var;
    } bytes;
};

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    SignedShort c;

    c.a = -6;
    printf("\n\tЧисло a = %d = 0b%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n\n", c.a, c.bytes.byte1Var.b15, c.bytes.byte1Var.b14, c.bytes.byte1Var.b13, c.bytes.byte1Var.b12, c.bytes.byte1Var.b11, c.bytes.byte1Var.b10, c.bytes.byte1Var.b9, c.bytes.byte1Var.b8, c.bytes.byte0Var.b7, c.bytes.byte0Var.b6, c.bytes.byte0Var.b5, c.bytes.byte0Var.b4, c.bytes.byte0Var.b3, c.bytes.byte0Var.b2, c.bytes.byte0Var.b1, c.bytes.byte0Var.b0);
   
    if ((c.bytes.byte1Var.b15 ^ 0) == 0)
    {
        if ((c.a ^ 0) == 0)
        {
            printf("\tЧисло %d дорівнює 0\n", c.a);
        }
        else
        {
            printf("\tЧисло %d додатне\n", c.a);

        }
    }
    else if ((c.bytes.byte1Var.b15 ^ 0) == 1)
    {
        printf("\tЧисло %d від'ємне\n", c.a);
    }


    c.a = 16;
    printf("\n\tЧисло a = %d = 0b%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n\n", c.a, c.bytes.byte1Var.b15, c.bytes.byte1Var.b14, c.bytes.byte1Var.b13, c.bytes.byte1Var.b12, c.bytes.byte1Var.b11, c.bytes.byte1Var.b10, c.bytes.byte1Var.b9, c.bytes.byte1Var.b8, c.bytes.byte0Var.b7, c.bytes.byte0Var.b6, c.bytes.byte0Var.b5, c.bytes.byte0Var.b4, c.bytes.byte0Var.b3, c.bytes.byte0Var.b2, c.bytes.byte0Var.b1, c.bytes.byte0Var.b0);

    if ((c.bytes.byte1Var.b15 ^ 0) == 0)
    {
        if ((c.a ^ 0) == 0)
        {
            printf("\tЧисло %d дорівнює 0\n", c.a);
        }
        else
        {
            printf("\tЧисло %d додатне\n", c.a);

        }
    }
    else if ((c.bytes.byte1Var.b15 ^ 0) == 1)
    {
        printf("\tЧисло %d від'ємне\n", c.a);
    }
}
