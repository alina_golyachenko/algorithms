﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AR_Lab7_8
{
    class Program
    {
        //массивчик пути в города
        public static int[] dist = new int[19];
        public static bool[] visited = new bool[19];
        public static int[,] graph = new int[19, 19];

        public static void DFS(int currentCity, int cityIndex, string route, int distance = 0)
        {
            //если это не первый город маршрута, добавляем городок к маршруту
            if (currentCity != 0)
            {
                route += $" --> {cities[currentCity]}";
                distance += graph[cityIndex, currentCity];
            }
            else
            {
                //если первый, просто пишем первый городок
                route = $"{cities[cityIndex]}";
            }
            //проеверяем, являются ли города соседними, чтобы вывести расстояние
            if (graph[cityIndex, currentCity] != 0)
            {
                Console.WriteLine($"\t{route} \n\tПройдений шлях: {distance}\n\n");
            }
            //поиск в глубину
            //проверяем, есть ли у города сосед
            for (int i = 0; i < cities.Length; i++)
            {
                if (graph[currentCity, i] != 0)
                {
                    //если да, едем в тот город и проверяем, можно ли из него ещё куда-то приехать
                    DFS(i, currentCity, route, distance);
                }
            }
          
        }
        public static void BFS(int row = 0, int col = 0)
        {
            Console.WriteLine("\n");

            int currentCity = 0;
            visited[currentCity] = true;

            Queue<int> queue = new Queue<int>();
            queue.Enqueue(currentCity);

            while (queue.Count != 0)
            {
                //вытаскиваем из очереди город
                currentCity = queue.Dequeue();
                for (int i = 0; i < cities.Length; i++)
                {
                    //проверяем его соседей, с которыми он соединён рёбрам
                    //если таковые имеются, и ещё не были посещены
                    if (graph[currentCity, i] != 0 && !visited[i])
                    {
                        //отмечаем соседа как посещённого
                        visited[i] = true;
                        //пишем его в очередь, чтобы от него идти дальше
                        //как в поиске пути в алгоритме Дейкстры
                        queue.Enqueue(i);
                        //путь к текущему городу равен сумме уже пройденого пути
                        //(чтобы доехать до нынешней точки) и пути к соседу.
                        dist[i] = dist[currentCity] + graph[currentCity, i];
                        Console.WriteLine($"\tКиїв --> {cities[i]} -- {dist[i]} км");
                    }
                }
            }
        }

        public static void PrintAdjMatrix()
        {
            Console.Write($"\n\t");

            for (int i = 0; i < graph.GetLength(1); i++)
            {
                Console.Write($"{i,7}");
            }
            Console.WriteLine("\n");
            for (int i = 0; i < graph.GetLength(0); i++)
            {
                Console.Write($"\t{i}");
                for (int j = 0; j < graph.GetLength(1); j++)
                {
                    Console.Write($"{graph[i, j],7}");
                }
                Console.WriteLine();
            }
        }

        public static string[] cities = new string[]
        {
                "Київ", //0
                "Житомир", //1
                "Новоград-Волинський", //2
                "Рівне", //3
                "Луцьк", //4
                "Бердичів", //5
                "Вінниця", //6
                "Хмельницький", //7
                "Тернопіль", //8
                "Шепетівка", //9
                "Біла Церква", //10
                "Умань", //11
                "Черкаси", //12
                "Кременчуг", //13
                "Полтава", //14
                "Харків", //15
                "Прилуки", //16
                "Суми", //17
                "Миргород" //18
        };
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            //Kyiv
            graph[0, 1] = 135;
            graph[0, 10] = 78;
            graph[0, 16] = 128;

            //Zhytoyr
            graph[1, 2] = 80;
            graph[1, 5] = 38;
            graph[1, 9] = 115;

            //Novograd-Volynskyi
            graph[2, 3] = 100;

            //Rivne
            graph[3, 4] = 68;

            //Berdychiv
            graph[5, 6] = 73;

            //Vinnytsa
            graph[6, 7] = 110;

            //Khmelnytskiy
            graph[7, 8] = 104;

            //Bila Tserkva
            graph[10, 11] = 115;
            graph[10, 12] = 146;
            graph[10, 14] = 181;

            //Cherkasy
            graph[12, 13] = 105;

            //Poltava
            graph[14, 15] = 130;

            //Pryluky
            graph[16, 17] = 175;
            graph[16, 18] = 109;

            PrintAdjMatrix();

            Console.WriteLine("\n\n\tПошук у ширину:\n");

            BFS();

            Console.WriteLine("\n\n\tПошук у глибину:\n");

            string str = "";
            DFS(0, 0, str);

        }
    }
}