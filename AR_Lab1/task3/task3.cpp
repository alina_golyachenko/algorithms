﻿#include <stdio.h>
#include <windows.h>

union SignedChar
{
    signed char a;
    struct
    {
        unsigned char b0 : 1;
        unsigned char b1 : 1;
        unsigned char b2 : 1;
        unsigned char b3 : 1;
        unsigned char b4 : 1;
        unsigned char b5 : 1;
        unsigned char b6 : 1;
        unsigned char b7 : 1;

    } b;
};

union UnsignedChar
{
    signed char k;
    struct
    {
        unsigned char m0 : 1;
        unsigned char m1 : 1;
        unsigned char m2 : 1;
        unsigned char m3 : 1;
        unsigned char m4 : 1;
        unsigned char m5 : 1;
        unsigned char m6 : 1;
        unsigned char m7 : 1;

    } l;
};

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    signed char num1, num2, res;

    //5 + 127
    num1 = 5;
    num2 = 127;
    res = num1 + num2;
    printf("signed char c = %d + %d = %d\n", num1, num2, res);

    SignedChar sch;
    sch.a = num1;
    printf("signed char num1 = %d = 0b%d%d%d%d%d%d%d%d\n", num1, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    sch.a = num2;
    printf("signed char num2 = %d = 0b%d%d%d%d%d%d%d%d\n", num2, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    sch.a += num1;
    printf("signed char res = %d = 0b%d%d%d%d%d%d%d%d\n", sch.a, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    printf("\n\n");
    
    //2 - 3

    num1 = 2;
    num2 = 3;
    res = num1 - num2;
    printf("signed char c = %d - %d = %d\n", num1, num2, res);

    sch.a = num1;
    printf("signed char num1 = %d = 0b%d%d%d%d%d%d%d%d\n", num1, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    sch.a = num2;
    printf("signed char num2 = %d = 0b%d%d%d%d%d%d%d%d\n", num2, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    sch.a = num1 - num2;
    printf("signed char res = %d = 0b%d%d%d%d%d%d%d%d\n", sch.a, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    //-120 - 34

    printf("\n\n");

    num1 = -120;
    num2 = 34;
    res = num1 - num2;
    printf("signed char c = %d - %d = %d\n", num1, num2, res);

    sch.a = num1;
    printf("signed char num1 = %d = 0b%d%d%d%d%d%d%d%d\n", num1, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    sch.a = num2;
    printf("signed char num2 = %d = 0b%d%d%d%d%d%d%d%d\n", num2, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    sch.a = num1 - num2;
    printf("signed char res = %d = 0b%d%d%d%d%d%d%d%d\n", sch.a, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);


    printf("\n\n");

    //unsigned char -5
    UnsignedChar uch;
    unsigned char x = -5;
    uch.k = x;
    printf("unsigned char x = %d = 0b%d%d%d%d%d%d%d%d\n", x, uch.l.m7, uch.l.m6, uch.l.m5, uch.l.m4, uch.l.m3, uch.l.m2, uch.l.m1, uch.l.m0);

    printf("\n\n");

    //56 & 38

        //56 & 38 = 00111000 & 
        //          00100110
        //          =
        //          00100000

    num1 = 56;
    num2 = 38;
    res = num1 & num2;
    printf("signed char c = %d & %d = %d\n", num1, num2, res);

    sch.a = num1;
    printf("signed char num1 = %d = 0b%d%d%d%d%d%d%d%d\n", num1, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    sch.a = num2;
    printf("signed char num2 = %d = 0b%d%d%d%d%d%d%d%d\n", num2, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    sch.a = num1 & num2;
    printf("signed char res = %d = 0b%d%d%d%d%d%d%d%d\n", sch.a, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);


    printf("\n\n");

    //56 | 38 = 00111000 | 
    //          00100110
    //          =
    //          00111110

    num1 = 56;
    num2 = 38;
    res = num1 | num2;
    printf("signed char c = %d | %d = %d\n", num1, num2, res);

    sch.a = num1;
    printf("signed char num1 = %d = 0b%d%d%d%d%d%d%d%d\n", num1, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    sch.a = num2;
    printf("signed char num2 = %d = 0b%d%d%d%d%d%d%d%d\n", num2, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);

    sch.a = num1 | num2;
    printf("signed char res = %d = 0b%d%d%d%d%d%d%d%d\n", sch.a, sch.b.b7, sch.b.b6, sch.b.b5, sch.b.b4, sch.b.b3, sch.b.b2, sch.b.b1, sch.b.b0);


    printf("\n\n");

}
