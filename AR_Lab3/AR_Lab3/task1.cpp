﻿#include <iostream>
#include<windows.h>
#include <chrono>
#include<math.h>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>


int Faktorial(int n)
{
    if (n >= 0)
    {
        if (n == 0)
        {
            return 1;
        }
        else
        {
            return n * Faktorial(n - 1);
        }
    }
}

int main()
{    
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int n;
    double f;
    printf("\n\t1 - f(n) = n\n\t2 - f(n) = log(n)\n\t3 - f(n) = n * log(n)\n\t4 - f(n) = n^2\n\t5 - f(n) = 2^n\n\t6 - f(n) = n!\n");
    printf("\n\tОберіть функцію, яку будете табулювати: ");
    int k;
    scanf_s("%d", &k);
    printf("\n");

    switch (k)
    {
        case 1:
        {
            for (int n = 0; n <= 50; n++)
            {
                f = n;
                if (f > 500)
                {
                    break;
                }
                printf("\n\tn = %d, f(n) = %lf", n, f);
            }
            break;
        }
        case 2:
        {
            for (int n = 0; n <= 50; n++)
            {
                if (n == 0)
                {
                    printf("\n\tn = %d, Такого логарифму не існує", n);
                }
                else
                {
                    f = log(n);
                    if (f > 500)
                    {
                        break;
                    }
                    printf("\n\tn = %d, f(n) = %lf", n, f);
                }
            }
            break;
        }
        case 3:
        {
            for (int n = 0; n <= 50; n++)
            {
                if (n == 0)
                {
                    printf("\n\tn = %d, Такого логарифму не існує", n);
                }    
                else
                {
                    f = n * log(n);
                    if (f > 500)
                    {
                        break;
                    }
                    printf("\n\tn = %d, f(n) = %lf", n, f);
                }
            }
            break;
        }
        case 4:
        {
            for (int n = 0; n <= 50; n++)
            {
                f = pow(n, 2);
                if (f > 500)
                {
                    break;
                }
                printf("\n\tn = %d, f(n) = %lf", n, f);
            }
            break;
        }
        case 5:
        {
            for (int n = 0; n <= 50; n++)
            {
                f = pow(2, n);
                if (f > 500)
                {
                    break;
                }
                printf("\n\tn = %d, f(n) = %lf", n, f);
            }
            break;
        }
        case 6:
        {
            for (int n = 0; n <= 50; n++)
            {
                f = Faktorial(n);
                if (f > 500)
                {
                    break;
                }
                printf("\n\tn = %d, f(n) = %lf\n", n, f);
            }
            break;
        }
        default:
        {
            printf("\n\tТакої функції немає");

            break;
        }
    }   

    printf("\n\n");

}




